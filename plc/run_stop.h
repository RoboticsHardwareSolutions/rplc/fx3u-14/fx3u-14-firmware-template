#ifndef __RUN_STOP_H_
#define __RUN_STOP_H_


#include "stm32f1xx_hal.h"
#include "stdbool.h"
#include "main.h"

inline bool is_running(void) { return HAL_GPIO_ReadPin(RUN_SWITCH_GPIO_Port,RUN_SWITCH_Pin) == GPIO_PIN_SET; }

#endif