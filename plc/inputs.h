#ifndef __INPUTS_H_
#define __INPUTS_H_

#include "stm32f1xx_hal.h"
#include "stdbool.h"

bool input_X00(void);

bool input_X01(void);

bool input_X02(void);

bool input_X03(void);

bool input_X04(void);

bool input_X05(void);

bool input_X06(void);

bool input_X07(void);



#endif
