#include "outs.h"
#include "stm32f1xx_hal.h"
#include "main.h"

void out_Y00_set(void){
    HAL_GPIO_WritePin(OUT_Y00_GPIO_Port,OUT_Y00_Pin,GPIO_PIN_SET);
}

void out_Y00_reset(void){
    HAL_GPIO_WritePin(OUT_Y00_GPIO_Port,OUT_Y00_Pin,GPIO_PIN_RESET);
}

void out_Y01_set(void){
    HAL_GPIO_WritePin(OUT_Y01_GPIO_Port,OUT_Y01_Pin,GPIO_PIN_SET);
}

void out_Y01_reset(void){
    HAL_GPIO_WritePin(OUT_Y01_GPIO_Port,OUT_Y01_Pin,GPIO_PIN_RESET);
}

void out_Y02_set(void){
    HAL_GPIO_WritePin(OUT_Y02_GPIO_Port,OUT_Y02_Pin,GPIO_PIN_SET);
}

void out_Y02_reset(void){
    HAL_GPIO_WritePin(OUT_Y02_GPIO_Port,OUT_Y02_Pin,GPIO_PIN_RESET);
}

void out_Y03_set(void){
    HAL_GPIO_WritePin(OUT_Y03_GPIO_Port,OUT_Y03_Pin,GPIO_PIN_SET);
}

void out_Y03_reset(void){
    HAL_GPIO_WritePin(OUT_Y03_GPIO_Port,OUT_Y03_Pin,GPIO_PIN_RESET);
}

void out_Y04_set(void){
    HAL_GPIO_WritePin(OUT_Y04_GPIO_Port,OUT_Y04_Pin,GPIO_PIN_SET);
}

void out_Y04_reset(void){
    HAL_GPIO_WritePin(OUT_Y04_GPIO_Port,OUT_Y04_Pin,GPIO_PIN_RESET);
}

void out_Y05_set(void){
    HAL_GPIO_WritePin(OUT_Y05_GPIO_Port,OUT_Y05_Pin,GPIO_PIN_SET);
}

void out_Y05_reset(void){
    HAL_GPIO_WritePin(OUT_Y05_GPIO_Port,OUT_Y05_Pin,GPIO_PIN_RESET);
}
