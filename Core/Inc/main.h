/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TRIM2_ADC_Pin GPIO_PIN_2
#define TRIM2_ADC_GPIO_Port GPIOC
#define TRIM1_ADC_Pin GPIO_PIN_3
#define TRIM1_ADC_GPIO_Port GPIOC
#define OUT_Y03_Pin GPIO_PIN_0
#define OUT_Y03_GPIO_Port GPIOA
#define OUT_DA0_Pin GPIO_PIN_4
#define OUT_DA0_GPIO_Port GPIOA
#define OUT_DA1_Pin GPIO_PIN_5
#define OUT_DA1_GPIO_Port GPIOA
#define RUN_SWITCH_Pin GPIO_PIN_2
#define RUN_SWITCH_GPIO_Port GPIOB
#define INPUT_X06_Pin GPIO_PIN_13
#define INPUT_X06_GPIO_Port GPIOE
#define INPUT_X07_Pin GPIO_PIN_14
#define INPUT_X07_GPIO_Port GPIOE
#define INPUT_X04_Pin GPIO_PIN_15
#define INPUT_X04_GPIO_Port GPIOE
#define INPUT_X05_Pin GPIO_PIN_10
#define INPUT_X05_GPIO_Port GPIOB
#define INPUT_X02_Pin GPIO_PIN_11
#define INPUT_X02_GPIO_Port GPIOB
#define INPUT_X03_Pin GPIO_PIN_12
#define INPUT_X03_GPIO_Port GPIOB
#define INPUT_X00_Pin GPIO_PIN_13
#define INPUT_X00_GPIO_Port GPIOB
#define INPUT_X01_Pin GPIO_PIN_14
#define INPUT_X01_GPIO_Port GPIOB
#define LED_RUN_Pin GPIO_PIN_10
#define LED_RUN_GPIO_Port GPIOD
#define OUT_Y05_Pin GPIO_PIN_12
#define OUT_Y05_GPIO_Port GPIOD
#define OUT_Y01_Pin GPIO_PIN_8
#define OUT_Y01_GPIO_Port GPIOC
#define OUT_Y00_Pin GPIO_PIN_9
#define OUT_Y00_GPIO_Port GPIOC
#define OUT_Y02_Pin GPIO_PIN_8
#define OUT_Y02_GPIO_Port GPIOA
#define RS232_UART_TX_Pin GPIO_PIN_9
#define RS232_UART_TX_GPIO_Port GPIOA
#define RS232_UART_RX_Pin GPIO_PIN_10
#define RS232_UART_RX_GPIO_Port GPIOA
#define SYS_SDIO_EEPROM_Pin GPIO_PIN_13
#define SYS_SDIO_EEPROM_GPIO_Port GPIOA
#define SYS_SWCLK_RS485_DD_Pin GPIO_PIN_14
#define SYS_SWCLK_RS485_DD_GPIO_Port GPIOA
#define EEPROM_Pin GPIO_PIN_15
#define EEPROM_GPIO_Port GPIOA
#define RS485_UART_TX_Pin GPIO_PIN_10
#define RS485_UART_TX_GPIO_Port GPIOC
#define RS485_UART_RX_Pin GPIO_PIN_11
#define RS485_UART_RX_GPIO_Port GPIOC
#define OUT_Y04_Pin GPIO_PIN_3
#define OUT_Y04_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
